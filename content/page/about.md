---
title: Over mij
subtitle: Why you'd want to hang out with me
comments: false
---

Als beginnende CMD’er vind ik het ontzettend leuk om me bezig te houden met vormgeving en communicatie. Uiteraard was dit ook de reden dat ik ben begonnen aan deze studie. Tijdens mijn studie wil ik me vooral bezig houden met visual design en daar wil ik hierna ook zeker mijn fulltime baan van gaan maken. Het combineren van mijn creativiteit en mijn passie voor fotografie is iets wat ik voortdurend wil ontwikkelen. Het beginnen van een eigen bedrijf begint ook steeds meer een droom van mij te worden en wil ik zeker werkelijkheid gaan maken.
