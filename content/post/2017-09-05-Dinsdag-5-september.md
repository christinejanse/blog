# Dinsdag 5 september 
Vandaag werd er een hoorcollege gegeven over het ontwerpproces. Er werd uitgelegd wat design is, wat het verschil is en dat het een vaardigheid is. Ook werd het verschil uitgelegd tussen design en kunst. 
Design is een proces waarbij je verschillende stappen doorloopt. Soms kan het zo zijn dat de context van het probleem veranderd waardoor de bedachte oplossing niet meer relevant is. Het kan dus soms onduidelijk en onvoorspelbaar zijn. Dit noem je ook wel ‘wicked problems’. 


