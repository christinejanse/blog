# Dinsdag 12 september
Vandaag kregen we een hoorcollege over verbeelden. Het ging vooral over vormen van visuele communicatie. Dit is onder te verdelen in drie niveaus: leren kijken, leren begrijpen en interpreteren en beelden kiezen en maken. Dit werd verder uitgelegd aan de hand van verschillende wetten. Bijvoorbeeld de wet van eenvoud en continuïteit. 

![aantekeningen hoorcollege](https://ibb.co/iH5fEQ)

Ik vond het interessant om te weten te komen hoe je onbewust aangetrokken kan worden tot een beeld. Gebruik maken van al deze wetten, soorten tekens en manieren van overtuigen is heel belangrijk bij het ontwerpen. 
