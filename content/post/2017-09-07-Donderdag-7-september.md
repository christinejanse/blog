# Donderdag 7 september


Vandaag kregen we ons eerste werkcollege. Er werd verder ingegaan op de stof die in het hoorcollege van deze week werd behandeld. 
Eerst moesten we vanuit een eigen ervaring een ontwerpproces visualiseren. Dus een moment waarop we een bestaande situatie in een gewenste situatie hebben omgezet. Bij deze opdracht kwam ik erachter dat ik dit ontwerpproces al vaker ben doorlopen zonder dat ik er bewust mee bezig was. Hierdoor kwam ik er ook achter dat je de stappen die je maakt bij een ontwerpproces op heel veel verschillende dingen kan toepassen. Niet alleen bij het maken van een app, maar ook bij het organiseren van een feest of vakantie. 

-FOTO EIGEN PROCES-

We hebben binnen ons team van elk persoon de visualisatie besproken. Daarna hebben we het beste concept gekozen en die verder uitgewerkt. 
Dit concept hebben twee mensen van ons team gepresenteerd waarbij andere teams feedback op ons gekozen concept gaven. De teampresentaties waren erg leerzaam doordat er feedback werd gegeven op elk concept. Hierdoor werd voor mij ook steeds meer duidelijk wat een ‘wicked problem’ precies is en wat de ‘problem space’ en de ‘solution space’ daarmee te maken heeft.
Voor mij was dit werkcollege een goede aanvulling op het hoorcollege waardoor ik het geheel nu beter begrijp. (FOTO POSTER)
Deze dag begon met een stand-up; ieder groepje gaf uitleg over de status van hun project. Hierdoor kreeg je een beetje een idee hoe ver iedereen was en wat hun ideeën zijn. Daarna kregen we een verdere uitleg over de deliverables.
We kwamen erachter dat sommige dingen wat chaotisch gingen. We hebben besproken wat we vandaag gingen doen. We hebben de onderzoeksresultaten van de enquête die in het weekend gemaakt is bekeken. We hebben duidelijk opgeschreven wat ons concept precies inhoud en wat de spelregels zijn. Samen met Benny heb ik de teaminventarisatie gedaan. We kwamen erachter dat ons visueel onderzoek naar de doelgroep niet helemaal goed is dus die gaan we opnieuw maken. 
Ook hebben we met Mio gesproken over onze ideeën en over de enquête. Er was een enquête gemaakt over ons concept, maar eigenlijk hoorde dit pas bij iteratie 2.
Vandaag heb ik geleerd dat je bij de kringloop van het ontwerpproces je bij ieder punt kan beginnen, maar je moet wel alle stappen gedaan hebben. Dit is eigenlijk wat wij ook hebben gedaan bij ons project. We hadden een concept, en hebben daarna onderzoek  gedaan naar onze doelgroep. 
